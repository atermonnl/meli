<?php

namespace Drupal\field_quiz\Form;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Url;

/**
 * Provides a webform client form.
 */
class FieldQuizAnswerForm extends FormBase {

  /**
   * The entity the client form belongs to.
   *
   * @var \Drupal\Core\Entity\ContentEntityBase
   */
  protected $entity;

  /**
   * The field items/ the questions.
   *
   * @var \Drupal\Core\Field\FieldItemList
   */
  protected $items;

  /**
   * FieldQuizAnswerForm constructor.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   The entity the client form belongs to.
   * @param \Drupal\Core\Field\FieldItemList $items
   *   The node the client form belongs to.
   */
  public function __construct(ContentEntityBase $entity, FieldItemList $items) {
    $this->entity = $entity;
    $this->items = $items;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_quiz_answer_form';
  }

  /**
   * Sort an array by random (shuffle) but preserve the keys.
   *
   * @param \Drupal\Core\Field\FieldItemList $items
   *   The items.
   *
   * @return \Drupal\Core\Field\FieldItemList
   *   The items.
   */
  private function shuffleAnswersAssoc(FieldItemList $items) {

    if (!empty($items)) {
      foreach ($items as $delta => $item) {
        $sort_array[$delta] = $item;
      }
      unset($items);
      if (!empty($sort_array)) {
        $new_sorted_array = [];
        $keys = array_keys($sort_array);
        if (!empty($keys)) {
          shuffle($keys);
          foreach ($keys as $key) {
            $new_sorted_array[$key] = $sort_array[$key];
          }
        }
        if (!empty($new_sorted_array)) {
          foreach ($new_sorted_array as $delta => $item) {
            $items[$delta] = $item;
          }
        }
      }
    }
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $entity = $this->entity;
    $form['#entity'] = $entity;
    $items = $this->items;
    $form['#items'] = $items;

    if (!empty($items)) {
      $config = $this->config('field_quiz.settings');

      // Shuffle questions if set in the administration settings.
      if (!empty($config->get('shuffle_answers'))) {
        // Sort the answers each time by random.
        $items = $this->shuffleAnswersAssoc($items);
      }

      $show_button = FALSE;
      foreach ($items as $delta => $item) {
        $values = $values = $item->getValue();
        if (!empty($values['answer'])) {
          $show_button = TRUE;
          // Provide each answer together with the checkbox.
          $form['answers']['item_' . $delta] = [
            '#type' => 'checkbox',
            '#default_value' => 0,
            '#title' => $values['answer'],
          ];
        }
      }

      if ($show_button) {
        $form['submit'] = [
          '#type' => 'submit',
          '#value' => t('Submit'),
        ];
      }

    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_state_values = $form_state->cleanValues()->getValues();

    if (!empty($form_state_values)) {

      $error = FALSE;

      $items = $form['#items'];
      $entity = $form['#entity'];

      if (!empty($items)) {

        foreach ($items as $delta => $item) {

          $item_values = $item->getValue();

          // Compare the submitted answers with the correct answers.
          // All correct answers should be answered correct -
          // there are no differences between correct and given answers allowed.
          // If the answer is available.
          if (isset($form_state_values['item_' . $delta])) {
            // If the answer is different with the correct one.
            if ($form_state_values['item_' . $delta] != $item_values['correct']) {
              $error = TRUE;
            }
          }
          else {
            // No answer available for this delta -
            // but should be there - error in the system.
            $error = TRUE;
          }

        }

      }

      // If user answered wrong.
      if ($error) {
        // Redirect the user to the sorry page.
        $url = new Url('field_quiz.sorry', array('entity' => $entity->id()));
        $form_state->setRedirectUrl($url);
      }
      else {
        // Redirect the user to the success page.
        $url = new Url('field_quiz.success', array('entity' => $entity->id()));
        $form_state->setRedirectUrl($url);
      }
    }

  }

}
