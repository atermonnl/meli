<?php

namespace Drupal\field_quiz\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Default controller for the field_quiz module.
 */
class FieldQuizController extends ControllerBase {

  /**
   * Text, if the user answered wrong.
   *
   * @return array
   *   The text.
   */
  public function sorry() {
    $config = $this->config('field_quiz.settings');
    $text = $config->get('test_answer_wrong');

    $elements[] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#attributes' => [
        'style' => 'color: red',
      ],
      '#value' => $text,
    ];

    return $elements;
  }

  /**
   * Text, if the user answered correctly.
   *
   * @return array
   *   The text.
   */
  public function success() {
    $config = $this->config('field_quiz.settings');
    $text = $config->get('test_answer_correct');

    $elements[] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#attributes' => [
        'style' => 'color: green',
      ],
      '#value' => $text,
    ];

    return $elements;
  }

}
