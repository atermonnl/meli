<?php

namespace Drupal\field_quiz\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_quiz_text' widget.
 *
 * @FieldWidget(
 *   id = "field_quiz_question_widget",
 *   module = "field_quiz",
 *   label = @Translation("Your multiple choice question widget."),
 *   field_types = {
 *     "field_quiz_question"
 *   }
 * )
 */
class TextWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $values = $items[$delta]->getValue();

    // Textfield answer.
    $element['answer'] = [
      '#type' => 'textfield',
      '#title' => t('Quiz answer'),
      '#description' => t('Enter an answer variant.'),
      '#default_value' => isset($values['answer']) ? $values['answer'] : NULL,
      '#size' => 50,
      '#maxlength' => 1024,
      '#element_validate' => [
        [$this, 'validate'],
      ],
      '#suffix' => '<div class="field-field-quiz"></div>',
      '#attributes' => [
        'class' => [
          'edit-field-field-quiz',
        ],
      ],
    ];

    // Checkbox correct answer.
    $element['correct'] = [
      '#type' => 'checkbox',
      '#title' => t('Correct answer?'),
      '#description' => t('Check this checkbox if this answer is correct.'),
      '#default_value' => isset($values['correct']) ? $values['correct'] : NULL,
      '#element_validate' => [
        [$this, 'validate'],
      ],
      '#suffix' => '<div class="field-field_quiz"></div>',
      '#attributes' => [
        'class' => [
          'edit-field-field-quiz',
        ],
      ],
    ];

    return $element;
  }

  /**
   * Validate the color text field.
   */
  public function validate($element, FormStateInterface $form_state) {

    // @todo Add Validation.
  }

}
