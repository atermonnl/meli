<?php

namespace Drupal\Tests\civiccookiecontrol\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * CivicCookieControl V8 tests.
 *
 * @group civiccookiecontrol
 */
class CookieControl8TestCase extends BrowserTestBase {

  /**
   * Admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['civiccookiecontrol', 'views'];

  /**
   *
   */
  public function setUp() {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser(['administer civiccookiecontrol']);

    $this->drupalLogin($this->adminUser);

  }

  /**
   *
   */
  public function testCCInit() {

    // $this->assertEqual($this->drupalUserIsLoggedIn($this->adminUser), TRUE);
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    // $this->assertText("Website working");
    $this->drupalGet('admin/config/system/cookiecontrol');
    $this->assertSession()->statusCodeEquals(200);
    // $this->assertText("CookieControl page loads");
    $ccconfig = \Drupal::configFactory()->getEditable('civiccookiecontrol.settings');

    $this->assertEqual($ccconfig->get('civiccookiecontrol_api_key'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_api_key_version'), 9);
    $this->assertEqual($ccconfig->get('civiccookiecontrol_product'), 'COMMUNITY');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_mode'), 'GDPR');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_popup'), TRUE);
    $this->assertEqual($ccconfig->get('civiccookiecontrol_same_site_cookie'), TRUE);
    $this->assertEqual($ccconfig->get('civiccookiecontrol_accept_behaviour'), 'all');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_close_on_global_change'), TRUE);
    $this->assertEqual($ccconfig->get('civiccookiecontrol_initial_state'), 'OPEN');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_notify_once'), FALSE);
    $this->assertEqual($ccconfig->get('civiccookiecontrol_reject_button'), FALSE);
    $this->assertEqual($ccconfig->get('civiccookiecontrol_layout'), 'SLIDEOUT');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_on_text'), 'On');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_off_text'), 'Off');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_accept_recommended'), 'Accept Recommended Settings');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_reject_settings'), 'Reject All');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_accept_text'), 'Accept');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_reject_text'), 'Reject');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_setting_text'), 'Cookie Preferences');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_widget_position'), 'LEFT');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_widget_theme'), 'LIGHT');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_remove_icon'), FALSE);
    $this->assertEqual($ccconfig->get('civiccookiecontrol_remove_about_text'), FALSE);
    $this->assertEqual($ccconfig->get('civiccookiecontrol_font_color'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_font_size_title'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_font_size_intro'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_font_size_headers'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_font_size'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_font_family'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_alert_text'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_alert_background'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_log_consent'), FALSE);
    $this->assertEqual($ccconfig->get('civiccookiecontrol_encode_cookie'), FALSE);
    $this->assertEqual($ccconfig->get('civiccookiecontrol_sub_domains'), TRUE);
    $this->assertEqual($ccconfig->get('civiccookiecontrol_background_color'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_accept_text_color'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_accept_background_color'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_toggle_text'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_toggle_color'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_toggle_background'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_button_icon'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_button_icon_width'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_button_icon_height'), '');
    $this->assertEqual($ccconfig->get("civiccookiecontrol_title_text"), 'This site uses cookies to store information on your computer.');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_intro_text'), 'Some of these cookies are essential to make our site work and others help us to improve by giving us some insight into how the site is being used.');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_necessary_title_text'), 'Necessary Cookies');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_necessary_desc_text'), 'Necessary cookies enable core functionality. The website cannot function properly without these cookies, and can only be disabled by changing your browser preferences.');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_notify_title_text'), 'Your choice regarding cookies on this site');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_notify_desc_text'), 'We use cookies to optimise site functionality and give you the best possible experience.');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_third_party_title_text'), 'Warning: Some cookies require your attention.');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_third_party_desc_text'), 'Consent for the following cookies could not be automatically revoked. Please follow the link(s) below to opt out manually.');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_consent_cookie_expiry'), '90');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_privacynode'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_debug'), FALSE);
    $this->assertEqual($ccconfig->get('civiccookiecontrol_stmt_descr'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_stmt_name'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_stmt_date'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_toggle_type'), 'slider');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_close_style'), 'icon');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_settings_style'), 'button');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_access_key'), 'C');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_highlight_focus'), FALSE);
    $this->assertEqual($ccconfig->get('civiccookiecontrol_onload'), '');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_close_label'), 'Close');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_accessibility_alert'), 'This site uses cookies to store information. Press accesskey C to learn more about your options.');
    $this->assertEqual($ccconfig->get('civiccookiecontrol_drupal_admin'), FALSE);

    // $this->assertSession()->fieldExists('civiccookiecontrol_api_key_version');
    $this->assertSession()->fieldExists('civiccookiecontrol_api_key');
    $this->assertSession()->fieldExists('civiccookiecontrol_product');

    $this->assertSession()->pageTextContains('Please provide a valid Cookie Control API key to proceed.');

    $this->submitForm([], 'edit-submit');

  }

  /**
   *
   */
  public function testCCV8() {
    $this->drupalGet('admin/config/system/cookiecontrol');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldExists('civiccookiecontrol_api_key_version');
    $this->assertSession()->fieldExists('civiccookiecontrol_api_key');
    $this->assertSession()->fieldExists('civiccookiecontrol_product');

    $ccpage = $this->getSession()->getPage();
    $ccpage->selectFieldOption('civiccookiecontrol_api_key_version', 8);
    $ccpage->fillField('civiccookiecontrol_api_key_version', 8);
    $ccpage->fillField('civiccookiecontrol_api_key', 'b8a7275b21225f07309796470c2eab3689c2dfce');
    $ccpage->fillField('civiccookiecontrol_product', 'PRO_MULTISITE');

    $this->submitForm([], 'edit-submit');

    $ccpage->fillField('civiccookiecontrol_product', 'COMMUNITY');
    $this->assertSession()->fieldExists('civiccookiecontrol_font_family');
    $ccpage->fillField('civiccookiecontrol_product', 'PRO_MULTISITE');
    $this->assertSession()->fieldExists('civiccookiecontrol_font_family');

    $this->assertSession()->fieldExists('civiccookiecontrol_log_consent');
    $this->assertSession()->fieldExists('civiccookiecontrol_encode_cookie');
    $this->assertSession()->fieldExists('civiccookiecontrol_sub_domains');
    $this->assertSession()->fieldExists('civiccookiecontrol_sub_domains');
    $this->assertSession()->fieldExists('civiccookiecontrol_initial_state');
    $this->assertSession()->fieldExists('civiccookiecontrol_layout');
    $this->assertSession()->fieldExists('civiccookiecontrol_notify_once');
    $this->assertSession()->fieldExists('civiccookiecontrol_reject_button');
    $this->assertSession()->fieldExists('civiccookiecontrol_toggle_type');
    $this->assertSession()->fieldExists('civiccookiecontrol_close_label');
    $this->assertSession()->fieldExists('civiccookiecontrol_settings_style');
    $this->assertSession()->fieldExists('civiccookiecontrol_consent_cookie_expiry');
    $this->assertSession()->fieldExists('civiccookiecontrol_widget_position');
    $this->assertSession()->fieldExists('civiccookiecontrol_widget_theme');
    $this->assertSession()->fieldExists('civiccookiecontrol_title_text');
    $this->assertSession()->fieldExists('civiccookiecontrol_intro_text');

    // $cc = $ccpage->findById('civiccookiecontrol_api_key_version');
    // print_r($ccpage->findField('civiccookiecontrol_api_key_version'));
    //
  }

  /**
   *
   */
  public function testCCV9() {

  }

}
