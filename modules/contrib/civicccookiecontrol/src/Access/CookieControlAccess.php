<?php

namespace Drupal\civiccookiecontrol\Access;

use Drupal\civiccookiecontrol\Form\CCCFormHelper;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Utility class implementing permission checks.
 */
class CookieControlAccess {

  /**
   * Function that checks if user has the perms to admin cookie control.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Current user account.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultNeutral
   *   Return value.
   */
  public function checkAccess(AccountInterface $account) {
    return AccessResult::allowedIf($account->hasPermission('administer civiccookiecontrol') && $this->checkApiKey());
  }

  /**
   * Function that checks if IAB1 access is allowed.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Current user account.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultNeutral
   *   Return value.
   */
  public function checkIab1Access(AccountInterface $account) {
    return AccessResult::allowedIf($account->hasPermission('administer civiccookiecontrol') && $this->checkApiKey() && (\Drupal::config('civiccookiecontrol.settings')->get('civiccookiecontrol_api_key_version') == 8));
  }

  /**
   * Function that checks if IAB2 access is allowed.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Current user account.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultNeutral
   *   Return value.
   */
  public function checkIab2Access(AccountInterface $account) {
    return AccessResult::allowedIf($account->hasPermission('administer civiccookiecontrol') && $this->checkApiKey() && (\Drupal::config('civiccookiecontrol.settings')->get('civiccookiecontrol_api_key_version') == 9));
  }

  /**
   * Function that checks if IAB2 access is enebled.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Current user account.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultNeutral
   *   Return value.
   */
  public function checkIab2EnabledAccess(AccountInterface $account) {
    if (\Drupal::config('civiccookiecontrol.settings')
      ->get('civiccookiecontrol_api_key_version') == 8) {
      return $this->checkIab1Access($account);
    }
    elseif (\Drupal::config('civiccookiecontrol.settings')
      ->get('civiccookiecontrol_api_key_version') == 9) {
      return AccessResult::allowedIf(
        $account->hasPermission('administer civiccookiecontrol') &&
        $this->checkApiKey() &&
        (\Drupal::config('iab2.settings')->get('iabCMP') == FALSE)
      );
    }
    return AccessResult::neutral();
  }

  /**
   * Function that checks if the cookie control api key is valid.
   *
   * @return bool
   *   Return value.
   */
  public static function checkApiKey() {
    if (CCCFormHelper::validateApiKey(\Drupal::config('civiccookiecontrol.settings')->get('civiccookiecontrol_api_key'), \Drupal::config('civiccookiecontrol.settings')->get('civiccookiecontrol_product')) ==
          \Drupal::config('civiccookiecontrol.settings')->get('civiccookiecontrol_product')) {
      return TRUE;
    }
    else {
      return FALSE;
    }

  }

}
