<?php

namespace Drupal\civiccookiecontrol\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Locale\CountryManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The configuration form for cookie control settings.
 */
class IAB2Settings extends ConfigFormBase {

  /**
   * Country manager object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\Drupal\Core\Locale\CountryManager
   */
  protected $countryManager;

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Cache backend object.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Router builder object.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routerBuilder;

  /**
   * {@inheritDoc}
   */
  public function __construct(CountryManager $countryManager, ConfigFactoryInterface $config, CacheBackendInterface $cache, RouteBuilderInterface $routeBuilder) {
    $this->countryManager = $countryManager;
    $this->config = $config->getEditable('iab2.settings');
    $this->cache = $cache;
    $this->routerBuilder = $routeBuilder;
    _check_cookie_categories();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('country_manager'),
      $container->get('config.factory'),
      $container->get('cache.data'),
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'iab2_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $configData = $this->config->get();
    foreach ($configData as $key => $configValue) {
      if (strpos($key, 'iab') !== FALSE) {
        if (is_array($form_state->getValue($key)) && array_key_exists('format', $form_state->getValue($key))) {
          $this->config->set($key, $form_state->getValue($key)['value']);
        }
        elseif (strpos($key, 'Text') !== FALSE) {
          if ($form_state->getValue($key) != '') {
            $this->config->set($key, str_replace([
              "\r\n",
              "\n",
              "\r",
            ], '', $form_state->getValue($key)))->save();
          }
        }
        else {
          $this->config->set($key, $form_state->getValue($key))->save();
        }
      }
    }
    $this->cache->delete('civiccookiecontrol_config');
    $this->routerBuilder->rebuild();
    drupal_flush_all_caches();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['iab2.settings'];
  }

  /**
   * Ajax based save of IAB v2.0 option value.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response object.
   */
  public function saveIab2Option(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $this->config->set('iabCMP', $form_state->getValue('iabCMP'))->save();
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['iab'] = [
      '#type' => 'details',
      '#title' => $this->t('IAB V2.0 Settings'),
      '#open' => TRUE,
    ];

    $form['iab']['iabCMP'] = [
      '#type' => 'radios',
      '#title' => $this->t("Enable IAB (TCF V2.2) Support."),
      '#options' => [
        TRUE => $this->t("Yes"),
        FALSE => $this->t('No'),
      ],
      '#ajax' => [
        'callback' => [$this, 'saveIab2Option'],
        'effect' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Updating IAB...'),
        ],
      ],
      '#default_value' => $this->config->get('iabCMP') ? 1 : 0,
      '#description' => $this->t("Whether or not Cookie Control supports the IAB's TCF v2.2."),
    ];

    $form['iab']['iabSettings'] = [
      '#type' => 'details',
      '#title' => $this->t('IAB Settings'),
      '#open' => FALSE,
      '#states' => [
        // Action to take.
        'invisible' => [
          ':input[name=iabCMP]' => [
            'value' => 0,
          ],
        ],
      ],
    ];

    $form['iab']['iabSettings']['iabLanguage'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IAB ISO Language Code'),
      '#description' => $this->t('Two letter ISO language code that should be used to display information about IAB purposes.'),
      '#default_value' => $this->config->get('iabLanguage'),
    ];

    $form['iab']['iabSettings']['iabPublisherCC'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IAB Country code'),
      '#description' => $this->t('The country code of the country that determines legislation of reference. Commonly, this corresponds to the country in which the publisher’s business entity is established.'),
      '#default_value' => $this->config->get('iabPublisherCC'),
    ];

    $form['iab']['iabSettings']['iabPanelTitle'] = [
      '#type' => 'textarea',
      '#title' => $this->t('IAB Panel Title'),
      '#description' => $this->t('The title of the IAB Panel'),
      '#default_value' => $this->config->get('iabPanelTitle'),
    ];

    $form['iab']['iabSettings']['iabPanelIntro1'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('IAB Panel Intro 1 Text'),
      '#description' => $this->t('IAB Panel Intro 1 Text'),
      '#default_value' => $this->config->get('iabPanelIntro1'),
    ];

    $form['iab']['iabSettings']['iabPanelIntro2'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('IAB Panel Intro 2 Text'),
      '#description' => $this->t('IAB Panel Intro 2 Text'),
      '#default_value' => $this->config->get('iabPanelIntro2'),
    ];

    $form['iab']['iabSettings']['iabPanelIntro3'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('IAB Panel Intro 3 Text'),
      '#description' => $this->t('IAB Panel Intro 3 Text'),
      '#default_value' => $this->config->get('iabPanelIntro3'),
    ];

    $form['iab']['iabSettings']['iabAboutIab'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('About IAB'),
      '#description' => $this->t('About IAB text area.'),
      '#default_value' => $this->config->get('iabAboutIab'),
    ];

    $form['iab']['iabSettings']['iabName'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IAB Name'),
      '#default_value' => $this->config->get('iabName'),
    ];

    $form['iab']['iabSettings']['iabLink'] = [
      '#type' => 'url',
      '#title' => $this->t('IAB Link'),
      '#description' => $this->t('Set the URL for IAB link.'),
      '#default_value' => $this->config->get('iabLink'),
    ];

    $form['iab']['iabSettings']['iabPurposes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IAB Purposes'),
      '#description' => $this->t('Set the purposes text.'),
      '#default_value' => $this->config->get('iabPurposes'),
    ];

    $form['iab']['iabSettings']['iabSpecialPurposes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IAB Special Purposes'),
      '#description' => $this->t('Set the special purposes text.'),
      '#default_value' => $this->config->get('iabSpecialPurposes'),
    ];

    $form['iab']['iabSettings']['iabFeatures'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IAB Features'),
      '#description' => $this->t('Set the features text.'),
      '#default_value' => $this->config->get('iabFeatures'),
    ];

    $form['iab']['iabSettings']['iabSpecialFeatures'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IAB Special Features'),
      '#description' => $this->t('Set the special features text.'),
      '#default_value' => $this->config->get('iabSpecialFeatures'),
    ];

    $form['iab']['iabSettings']['iabDataUse'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IAB Data use'),
      '#description' => $this->t('Set the data use text.'),
      '#default_value' => $this->config->get('iabDataUse'),
    ];

    $form['iab']['iabSettings']['iabVendors'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IAB Vendors'),
      '#description' => $this->t('Set the vendors text.'),
      '#default_value' => $this->config->get('iabVendors'),
    ];

    $form['iab']['iabSettings']['iabPurposeLegitimateInterest'] = [
      '#type' => 'textarea',
      '#title' => $this->t('IAB Purpose Legitimate Interest'),
      '#description' => $this->t('Set the purpose legitimate interest text.'),
      '#default_value' => $this->config->get('iabPurposeLegitimateInterest'),
    ];

    $form['iab']['iabSettings']['iabVendorLegitimateInterest'] = [
      '#type' => 'textarea',
      '#title' => $this->t('IAB Vendor Legitimate Interest'),
      '#description' => $this->t('Set the vendor legitimate interest text.'),
      '#default_value' => $this->config->get('iabVendorLegitimateInterest'),
    ];

    $form['iab']['iabSettings']['iabObjectPurposeLegitimateInterest'] = [
      '#type' => 'textarea',
      '#title' => $this->t('IAB Object Purpose Legitimate Interest'),
      '#description' => $this->t('Set the object purpose legitimate interest text.'),
      '#default_value' => $this->config->get('iabObjectPurposeLegitimateInterest'),
    ];

    $form['iab']['iabSettings']['iabObjectVendorLegitimateInterest'] = [
      '#type' => 'textarea',
      '#title' => $this->t('IAB Object Vendor Legitimate Interest'),
      '#description' => $this->t('Set the object vendor legitimate interest.'),
      '#default_value' => $this->config->get('iabObjectPurposeLegitimateInterest'),
    ];

    $form['iab']['iabSettings']['iabRelyConsent'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IAB Rely Consent'),
      '#description' => $this->t('Set the rely consent text.'),
      '#default_value' => $this->config->get('iabRelyConsent'),
    ];

    $form['iab']['iabSettings']['iabRelyLegitimateInterest'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IAB Rely Legitimate Interest'),
      '#description' => $this->t('Set the rely legitimate interest text.'),
      '#default_value' => $this->config->get('iabRelyLegitimateInterest'),
    ];

    $form['iab']['iabSettings']['iabSavePreferences'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IAB Save Preferences'),
      '#description' => $this->t('Set the save preferences text.'),
      '#default_value' => $this->config->get('iabSavePreferences'),
    ];

    $form['iab']['iabSettings']['iabAcceptAll'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IAB Accept All'),
      '#description' => $this->t('Set Accept All text.'),
      '#default_value' => $this->config->get('iabAcceptAll'),
    ];

    $form['iab']['iabSettings']['iabRejectAll'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IAB Reject All'),
      '#description' => $this->t('Set Reject All text.'),
      '#default_value' => $this->config->get('iabRejectAll'),
    ];

    $form['iab']['iabSettings']['iabDropDowns'] = [
      '#type' => 'radios',
      '#title' => $this->t("Hide Purposes, Special Purposes, Features and Special Features."),
      '#options' => [
        TRUE => $this->t("Yes"),
        FALSE => $this->t('No'),
      ],
      '#default_value' => $this->config->get('iabDropDowns') ? 1 : 0,
      '#description' => $this->t("If set to yes, Purposes, Special Purposes, Features and Special Features will be hidden by default so that the interface is more concise. The user will be able to see them after expanding the corresponding drop down."),
    ];

    $form['iab']['iabSettings']['iabFullLegalDescription'] = [
      '#type' => 'radios',
      '#title' => $this->t("Display full legal description for each Purpose or Feature."),
      '#options' => [
        TRUE => $this->t("Yes"),
        FALSE => $this->t('No'),
      ],
      '#default_value' => $this->config->get('iabFullLegalDescription') ? 1 : 0,
      '#description' => $this->t("If set to Yes, the full legal description for each Purpose or Feature will be shown, otherwise it will be hidden and the user can see them after expanding the corresponding drop down."),
    ];

    $form['iab']['iabSettings']['iabLegalDescription'] = [
      '#type' => 'textarea',
      '#title' => $this->t('IAB Legal Description Text'),
      '#description' => $this->t('Set the legal description text.'),
      '#default_value' => $this->config->get('iabLegalDescription'),
      '#states' => [
        // Action to take.
        'invisible' => [
          ':input[name=iabFullLegalDescription]' => [
            'value' => 1,
          ],
        ],
      ],
    ];
    $form['iab']['iabSettings']['iabSaveOnlyOnClose'] = [
      '#type' => 'radios',
      '#title' => $this->t("Save IAB settings on Close."),
      '#options' => [
        TRUE => $this->t("Yes"),
        FALSE => $this->t('No'),
      ],
      '#default_value' => $this->config->get('iabSaveOnlyOnClose') ? 1 : 0,
      '#description' => $this->t("Cookie Control will wait until the user closes the widget before saving the consent, rather than doing so every time the user toggles a purpose on or off."),
    ];

    $form_state->setCached(FALSE);

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save IAB (TCF v2.0) Configuration'),
      '#button_type' => 'primary',
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

}
