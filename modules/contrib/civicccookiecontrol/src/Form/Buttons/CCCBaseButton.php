<?php

namespace Drupal\civiccookiecontrol\Form\Buttons;

/**
 * Class CCCBaseButton.
 */
abstract class CCCBaseButton implements CCCButtonInterface {

  /**
   * {@inheritdoc}
   */
  public function ajaxify() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmitHandler() {
    return FALSE;
  }

}
