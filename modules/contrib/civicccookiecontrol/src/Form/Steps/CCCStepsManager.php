<?php

namespace Drupal\civiccookiecontrol\Form\Steps;

/**
 * Cookie control configuration form steps manager.
 */
class CCCStepsManager {

  /**
   * The array of steps.
   *
   * @var array
   */
  protected $steps;

  /**
   * Steps manager constructor.
   */
  public function __construct() {
  }

  /**
   * Add a step to the steps property.
   *
   * @param \Drupal\civiccookiecontrol\Form\Steps\CCCStepInterface $step
   *   Step of the form.
   */
  public function addStep(CCCStepInterface $step) {
    $this->steps[$step->getStep()] = $step;
  }

  /**
   * Fetches step from steps property, If it doesn't exist, create step object.
   *
   * @param int $step_id
   *   Step ID.
   *
   * @return \Drupal\civiccookiecontrol\Form\Steps\CCCStepInterface
   *   Return step object.
   */
  public function getStep($step_id) {
    if (isset($this->steps[$step_id])) {
      $step = $this->steps[$step_id];
    }
    else {
      $class = CCCStepsEnum::map($step_id);
      $step = \Drupal::service($class);
      $step->setStepManager($this);
    }

    return $step;
  }

  /**
   * Get all steps.
   *
   * @return array
   *   Steps.
   */
  public function getAllSteps() {
    return $this->steps;
  }

}
