## **BulkGate SMS Module**

Drupal integration of BulkGate SMS API into SMS Framework.
See more at https://www.bulkgate.com/

### Installation

Go to `/admin/modules` on your Drupal site 
and enable module **BulkGate SMS Module**

### Setup

In SMS Framework configuration add a new Gateway using BulkGate Gateway plugin.
Configure BulkGate-specific settings ('Application ID' and 'Application token').
If your credentials are correct, credit info should appear in the form after saving.

