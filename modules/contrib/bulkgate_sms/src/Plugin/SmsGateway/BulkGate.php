<?php

namespace Drupal\bulkgate_sms\Plugin\SmsGateway;

use BulkGate\Message\Connection;
use BulkGate\Sms\Message;
use BulkGate\Sms\Sender;
use BulkGate\Sms\SenderSettings\Gate;
use BulkGate\Sms\SenderSettings\StaticSenderSettings;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\sms\Message\SmsDeliveryReport;
use Drupal\sms\Message\SmsMessageInterface;
use Drupal\sms\Message\SmsMessageReportStatus;
use Drupal\sms\Message\SmsMessageResult;
use Drupal\sms\Message\SmsMessageResultStatus;
use Drupal\sms\Plugin\SmsGatewayPluginBase;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @SmsGateway(
 *   id = "bulkgate",
 *   label = @Translation("BulkGate"),
 *   outgoing_message_max_recipients = 1
 * )
 */
class BulkGate extends SmsGatewayPluginBase implements ContainerFactoryPluginInterface {

  protected $messenger;

  protected $httpClient;

  protected $datetime;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MessengerInterface $messenger,
    Client $client,
    TimeInterface $datetime
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->messenger = $messenger;
    $this->httpClient = $client;
    $this->datetime = $datetime;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('messenger'),
      $container->get('http_client'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'app_id' => '',
      'app_token' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $config = $this->getConfiguration();

    // If app_id and app_token were previously set, show credit balance
    if (!empty($config['app_id']) && !empty($config['app_token'])) {
      $data = $this->getCreditBalance();
      if (isset($data['data'])) {
        $data = $data['data'];
        $form['account_information'] = [
          '#type' => 'item',
          '#title' => $this->t('Wallet @wallet',['@wallet'=>$data['wallet']]),
          'credit' => [
            '#type' => 'item',
            '#title' => $this->t('Credit'),
            '#markup' => $data['credit']
          ],
          'currency' => [
            '#type' => 'item',
            '#title' => $this->t('Currency'),
            '#markup' => $this->t($data['currency'])
          ],
          'free_messages' => [
            '#type' => 'item',
            '#title' => $this->t('Free messages'),
            '#markup' => $data['free_messages']
          ],
        ];
      }else {
        $this->messenger->addWarning($this->t("Can't connect, check your Application ID and Token ID"));
      }
    }

    $form['app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Application ID'),
      '#default_value' => $config['app_id'],
      '#required' => TRUE
    ];

    $form['app_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Application token'),
      '#default_value' => $config['app_token'],
      '#required' => TRUE
    ];

    $form['static_sender_type'] = [
      '#type' => 'select',
      '#name' => 'static_sender_type',
      '#title' => t('Sender type'),
      '#empty_option' => t('None (system number)'),
      '#options' => [
        Gate::GATE_TEXT_SENDER => t('Text sender'),
      ],
      '#default_value' => $config['static_sender_type'],
    ];

    $form['static_sender_name'] = [
      '#type' => 'textfield',
      '#title' => t('Sender name'),
      '#default_value' => $config['static_sender_name'],
      '#states' => [
        'visible' => [
          ':input[name="static_sender_type"]' => ['value' => Gate::GATE_TEXT_SENDER],
        ],
        'required' => [
          ':input[name="static_sender_type"]' => ['value' => Gate::GATE_TEXT_SENDER],
        ],
      ]
    ];

    return $form;
  }

  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    if ($form_state->getValue('static_sender_type') == Gate::GATE_TEXT_SENDER && empty($form_state->getValue('static_sender_name'))) {
      $form_state->setErrorByName('static_sender_name', t('@field is required', ['@field' => $form['static_sender_name']['#title']]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['app_id'] = trim($form_state->getValue('app_id'));
    $this->configuration['app_token'] = trim($form_state->getValue('app_token'));
    $this->configuration['static_sender_type'] = $form_state->getValue('static_sender_type');
    $this->configuration['static_sender_name'] = $form_state->getValue('static_sender_name');
  }

  public function send(SmsMessageInterface $sms) {
    $config = $this->getConfiguration();
    $recipient = $sms->getRecipients()[0];
    // Send SMS
    $connection = new Connection($config['app_id'], $config['app_token'], 'https://portal.bulkgate.com/api/1.0/php-sdk', 'drupal');
    $sender = new Sender($connection);
    // If sender type is configured, add sender settings
    if (!empty($config['static_sender_type'])) {
      switch ($config['static_sender_type']) {
        case Gate::GATE_TEXT_SENDER:
          $value = $config['static_sender_name'];
          break;
      }
      $sender_settings = new StaticSenderSettings($config['static_sender_type'], $value);
      $sender->setSenderSettings($sender_settings);
    }

    $message = new Message($recipient, $sms->getMessage());
    $response = $sender->send($message);
    $result = new SmsMessageResult();
    $report = (new SmsDeliveryReport())
      ->setRecipient($recipient)
      ->setTimeDelivered($this->datetime->getRequestTime());
    if ($response->isSuccess()) {
      $report->setStatus(SmsMessageReportStatus::DELIVERED);
      $report->setStatusMessage('DELIVERED');
    }
    else {
      $result_status = SmsMessageResultStatus::ERROR;
      $report_status = SmsMessageReportStatus::ERROR;
      switch ($response->__get('code')) {
        case 400:
          $report_status = SmsMessageReportStatus::INVALID_RECIPIENT;
          break;
        case 401:
          $result_status = SmsMessageResultStatus::ACCOUNT_ERROR;
          break;
      }
      $result->setError($result_status);
      $report->setStatus($report_status);
    }
    $result->addReport($report);
    return $result;
  }

  public function getCreditBalance() {
    $config = $this->getConfiguration();
    $app_id = $config['app_id'];
    $app_token = $config['app_token'];
    $api_url = "https://portal.bulkgate.com/api/1.0/simple/info";
    $params = '?application_id='.$app_id.'&application_token='.$app_token;
    try {

      $response = $this->httpClient->get($api_url.$params);

      $result = json_decode($response->getBody()->getContents(),TRUE);
    }catch (ClientException $exception) {
      $result = json_decode($exception->getResponse()->getBody()->getContents(), TRUE);
    }

    return $result;
  }

}
