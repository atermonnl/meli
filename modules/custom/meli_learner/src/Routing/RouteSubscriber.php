<?php
/**
 * @file
 * Contains \Drupal\meli_learner\Routing\RouteSubscriber.
 */

namespace Drupal\meli_learner\Routing;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('multiple_registration.role_registration_page')) {
      $route->setDefault('_title_callback', '\Drupal\meli_learner\Routing\RouteSubscriber::userRegisterTitle');
    }
  }

  public function userRegisterTitle(RouteMatchInterface $route) {
    return t('Register');
  }

}
