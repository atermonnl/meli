<?php
namespace Drupal\reportasap_dd\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class ReportasapDDSettingsForm extends ConfigFormBase {
    /** @var string Config settings */
  const SETTINGS = 'reportasap_dd.settings';

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reportasap_dd_admin_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['pass_score_intro'] = array(
      '#type'          => 'text_format',
      '#title'         => t('Pass score introduction'),
      '#format'        => $config->get('pass_score_intro') ? $config->get('pass_score_intro')['format'] : 'basic_html',
      '#default_value' => $config->get('pass_score_intro') ? $config->get('pass_score_intro')['value'] : '',
      '#description'   => t('Pass score introductory text'),
    );

    $form['certificate_intro'] = array(
      '#type'          => 'text_format',
      '#title'         => t('Certificate introduction'),
      '#format'        => $config->get('certificate_intro') ? $config->get('certificate_intro')['format'] : 'basic_html',
      '#default_value' => $config->get('certificate_intro') ? $config->get('certificate_intro')['value'] : '',
      '#description'   => t('Certificate introductory text'),
    );

    $form['fail_score_intro'] = array(
      '#type'          => 'text_format',
      '#title'         => t('Fail score introduction'),
      '#format'        => $config->get('fail_score_intro') ? $config->get('fail_score_intro')['format'] : 'basic_html',
      '#default_value' => $config->get('fail_score_intro') ? $config->get('fail_score_intro')['value'] : '',
      '#description'   => t('Fail score introductory text'),
    );

    $form['tips_intro'] = array(
      '#type'          => 'text_format',
      '#title'         => t('Tips introduction'),
      '#format'        => $config->get('tips_intro') ? $config->get('tips_intro')['format'] : 'basic_html',
      '#default_value' => $config->get('tips_intro') ? $config->get('tips_intro')['value'] : '',
      '#description'   => t('Tips introductory text'),
    );
    
    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

       $this->configFactory->getEditable(static::SETTINGS)

      ->set('example_thing', $form_state->getValue('example_thing'))
      ->set('pass_score_intro', $form_state->getValue('pass_score_intro'))
      ->set('certificate_intro', $form_state->getValue('certificate_intro'))
      ->set('fail_score_intro', $form_state->getValue('fail_score_intro'))
      ->set('tips_intro', $form_state->getValue('tips_intro'))
      ->save();
      
    parent::submitForm($form, $form_state);
  }
}