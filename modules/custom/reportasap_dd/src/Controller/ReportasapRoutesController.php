<?php

namespace Drupal\reportasap_dd\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\user\PrivateTempStoreFactory;
use Drupal\webform\Entity\WebformSubmission;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
//use Drupal\Core\Language\LanguageInterface;


/**
 * ReportasapRoutesController controller.
 */
class ReportasapRoutesController extends ControllerBase {

  public function ddResults($sid) {
    
    $config = \Drupal::config('reportasap_dd.settings');
    $intro['pass_text'] = $config->get('pass_score_intro') ? $config->get('pass_score_intro')['value'] : '';
    $intro['fail_text'] = $config->get('fail_score_intro') ? $config->get('fail_score_intro')['value'] : '';
    $intro['certificate_text'] = $config->get('certificate_intro') ? $config->get('certificate_intro')['value'] : '';
    $intro['tips_text'] = $config->get('tips_intro') ? $config->get('tips_intro')['value'] : '';
    
    $uid = \Drupal::currentUser()->id();

    if (!is_valid_sid($sid)) {       
      return [
        '#theme' => 'dynamic_demonstrator_results',
        '#sid' => $sid,
        '#uid' => $uid,
      ];
    }
    
    $results = $this->_getddResults($sid);
    
    return [
      '#theme' => 'dynamic_demonstrator_results',
      '#sid' => $sid,
      '#uid' => $uid,
      '#pass' => $results['pass'],
      '#score' => $results['score'],
      '#tips' => $results['tips'],
      '#introtext_array' => $intro,
    ];
  }
  
  public function ddResultsLatest() {    
    
    $config = \Drupal::config('reportasap_dd.settings');
    $intro['pass_text'] = $config->get('pass_score_intro') ? $config->get('pass_score_intro')['value'] : '';
    $intro['fail_text'] = $config->get('fail_score_intro') ? $config->get('fail_score_intro')['value'] : '';
    $intro['certificate_text'] = $config->get('certificate_intro') ? $config->get('certificate_intro')['value'] : '';
    $intro['tips_text'] = $config->get('tips_intro') ? $config->get('tips_intro')['value'] : '';

    $uid = \Drupal::currentUser()->id();

    $query = \Drupal::entityQuery('webform_submission')
                                  ->condition('uid', $uid)
                                  ->condition('webform_id', 'dynamic_demonstrator')
                                  ->condition('in_draft', 0)
                                  ->sort('sid' , 'DESC')
                                  ->range(0,1);

    $sid = $query->execute();
    if ($sid && $uid){  
      $sid = array_shift($sid);
      
      $results = $this->_getddResults($sid);
    
      return [
        '#theme' => 'dynamic_demonstrator_results',
        '#sid' => $sid,
        '#uid' => $uid,
        '#pass' => $results['pass'],
        '#score' => $results['score'],
        '#tips' => $results['tips'],
        '#introtext_array' => $intro,
      ];
      
    }
    
    else {
       
      return [
        '#theme' => 'dynamic_demonstrator_results',
        '#sid' => $sid,
        '#uid' => $uid,
      ];
    }
  }
  
  public function userAccess(AccountInterface $account, $sid = NULL) {
    
    if (is_valid_sid($sid)) {
      $webform_submission = \Drupal\webform\Entity\WebformSubmission::load($sid);
      $uid = $webform_submission->getOwnerId();
    }
    
    if (($account->isAuthenticated()) 
        && $account->hasPermission('view own webform submission')
        && $account->id() == $uid){
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }
  
  protected function _getddResults($sid) {
    
    //$langcode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT);
        
    $webform = \Drupal\webform\Entity\Webform::load('dynamic_demonstrator');

    $webform_submission = \Drupal\webform\Entity\WebformSubmission::load($sid);

    $points = $score = 0;
    $pass = false;
    $results = $tips = [];
    
    if (!empty($webform_submission) && is_object($webform_submission)) {
      
      $answers = $webform_submission->getData();
      $total_answers = sizeof($answers);
      
      foreach ($answers as $key => $answer) {
        
        if ($answer === 'correct') {
          $points++; 
        }
        else {
          $tipid = explode('s', $key);
          /* If wrong answer get tip of this question -- ex. tip02 */
          $tips[$key] = $webform->getElement('tip' . $tipid[1])['#message_message'];
        }
      }
      
      $score = (100*$points)/$total_answers;
      
      $pass = $score >= 70 ? true : false;
    }
    
    $results['score'] = round($score);
    $results['pass'] = $pass;
    $results['tips'] = $tips;
    
    return $results;
  }
  
}