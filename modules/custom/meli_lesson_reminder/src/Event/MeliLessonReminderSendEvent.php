<?php

namespace Drupal\meli_lesson_reminder\Event;

use Drupal\profile\Entity\ProfileInterface;
use Symfony\Component\EventDispatcher\Event;

class MeliLessonReminderSendEvent extends Event {

  const EVENT_NAME = 'meli_lesson_reminder_send';

  /**
   * The user account.
   *
   * @var ProfileInterface
   */
  public $profile;

  /**
   * The activities list.
   *
   * @var string
   */
  public $lessons;

  /**
   * The activities list (shortened URLs).
   *
   * @var string
   */
  public $lessons_short;

  /**
   * Constructs the object.
   */
  public function __construct(ProfileInterface $profile, $lessons, $lessons_short) {
    $this->profile = $profile;
    $this->lessons = $lessons;
    $this->lessons_short = $lessons_short;
  }
}
