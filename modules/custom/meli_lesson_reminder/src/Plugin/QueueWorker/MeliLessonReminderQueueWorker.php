<?php

namespace Drupal\meli_lesson_reminder\Plugin\QueueWorker;

use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\meli_learner\MeliLearnerLanguagesManager;
use Drupal\meli_lesson_reminder\Event\MeliLessonReminderSendEvent;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

// Drupal\meli_lesson_reminder\Plugin\QueueWorker\MeliLessonReminderQueueWorker::sendReminder(46);

//    $entity_type = \Drupal::entityTypeManager()->getStorage('sms')->getEntityType();
//    \Drupal::entityDefinitionUpdateManager()->installEntityType($entity_type);

/**
 * @QueueWorker(
 * id = "meli_lesson_reminder",
 * title = @Translation("MELI Activity Reminder"),
 * cron = {"time" = 20}
 * )
 */
class MeliLessonReminderQueueWorker extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    // See https://niklan.net/blog/200.

    self::sendReminder($item['uid']);

  }

  // Drupal\meli_lesson_reminder\Plugin\QueueWorker\MeliLessonReminderQueueWorker::sendReminder();
  public static function sendReminder($uid) {

    /** @var \Drupal\profile\Entity\Profile $profile */
    $profile = meli_learner_load_profile($uid);

    if (!$profile) {
      return;
    }

    // Get list of nodes to send.
    if ($items = self::getLessons($uid)) {
      $links = [];
      $links_short = [];
      foreach ($items as $item) {
        $node = Node::load($item['nid']);
        $link = $node->toUrl('canonical', [
          'absolute' => TRUE,
          'language' => \Drupal::languageManager()
            ->getLanguage($item['langcode']),
        ])->toString();
        $links[] = $link;
        $links_short[] = shorten_url($link);
      }

      // Invoke rule to send.
      \Drupal::service('event_dispatcher')
        ->dispatch(MeliLessonReminderSendEvent::EVENT_NAME, new MeliLessonReminderSendEvent($profile, implode(', ', $links), implode(', ', $links_short)));

      // Update last send date.
      \Drupal::database()->merge('meli_lesson_reminder')
        ->key('uid', $uid)
        ->fields(['timestamp' => \Drupal::time()->getRequestTime()])
        ->execute();

      /** @var \Drupal\flag\FlagService $flag_service */
      $flag_service = \Drupal::service('flag');
      $flag = $flag_service->getFlagById('notified_lesson'); // replace by flag machine name

      /** @var User $account */
      $account = User::load($uid);

      foreach ($items as $item) {
        $node = Node::load($item['nid']);
        if (!$flag_service->getFlagging($flag, $node, $account)) {
          $flag_service->flag($flag, $node, $account);
        }
      }
    }

  }

  public static function getLessons($uid) {
    $query = \Drupal::database()->select('node_field_data', 'node_field_data');
    $query->distinct();

    $query->addExpression('node_field_data.langcode', 'langcode');
    $query->addExpression('node_field_data.nid', 'nid');
    $query->addExpression('node_field_data.created', 'node_field_data_created');

    $query->leftJoin('flagging', 'flagging_node_field_data', "node_field_data.nid = flagging_node_field_data.entity_id AND (flagging_node_field_data.flag_id = 'completed_lesson' AND flagging_node_field_data.uid = '$uid')");
    $query->leftJoin('flagging', 'flagging_node_field_data_1', "node_field_data.nid = flagging_node_field_data_1.entity_id AND (flagging_node_field_data_1.flag_id = 'favorite_lesson' AND flagging_node_field_data_1.uid = '$uid')");
    $query->leftJoin('flagging', 'flagging_node_field_data_2', "node_field_data.nid = flagging_node_field_data_2.entity_id AND (flagging_node_field_data_2.flag_id = 'notified_lesson' AND flagging_node_field_data_2.uid = '$uid')");
    $query->leftJoin('node__field_lesson_audience', 'node__field_lesson_audience_value_0', "node_field_data.nid = node__field_lesson_audience_value_0.entity_id");

    $query
      ->condition('node_field_data.status', '1')
      ->condition('node_field_data.type', 'lesson')
      ->isNull('flagging_node_field_data.uid')
      ->isNull('flagging_node_field_data_1.uid')
      ->isNull('flagging_node_field_data_2.uid')
      ->orderBy('node_field_data_created', 'ASC')
      ->range(0, 5);

    if ($profile = meli_learner_load_profile($uid)) {
      // Filter languages by country.
      $country_code = $profile->get('field_country')->getString();
      $query->condition('node_field_data.langcode', MeliLearnerLanguagesManager::getLanguagesFromCountryCode($country_code), 'IN');

      $or = new Condition('OR');
      $or->condition('node__field_lesson_audience_value_0.field_lesson_audience_value', 'Parents');

      // Filter by ages.
      if ($ages = $profile->get('field_children_age')->getValue()) {
        // Filter by those ages.
        $age_tids = [];
        foreach ($ages as $item) {
          $age_tids[] = $item['target_id'];
        }

        $and = new Condition('AND');

        $query->leftJoin('node__field_lesson_age_group', 'node__field_lesson_age_group_value_0', "node_field_data.nid = node__field_lesson_age_group_value_0.entity_id");

        $and->condition('node__field_lesson_audience_value_0.field_lesson_audience_value', 'Family');
        $and->condition('node__field_lesson_age_group_value_0.field_lesson_age_group_target_id', $age_tids, 'IN');
        $or->condition($and);
      }
    }
    $query->condition($or);

    $records = $query->execute();

    $items = [];
    foreach ($records as $record) {
      $items[] = ['nid' => $record->nid, 'langcode' => $record->langcode];
    }

    return $items;
  }
}
