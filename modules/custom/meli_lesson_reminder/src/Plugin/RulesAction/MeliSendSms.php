<?php

namespace Drupal\meli_lesson_reminder\Plugin\RulesAction;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\rules\Core\RulesActionBase;
use Drupal\sms\Direction;
use Drupal\sms\Entity\SmsMessage;
use Drupal\sms\Provider\SmsProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides "Send SMS" rules action.
 *
 * @RulesAction(
 *   id = "meli_send_sms",
 *   label = @Translation("Send SMS"),
 *   category = @Translation("SMS"),
 *   context_definitions = {
 *     "number" = @ContextDefinition("string",
 *       label = @Translation("Number"),
 *       description = @Translation("The number to send SMS to.")
 *     ),
 *     "message" = @ContextDefinition("text",
 *       label = @Translation("Message"),
 *       description = @Translation("The SMS message. Drupal will by default remove all HTML tags.")
 *     ),
 *     "language" = @ContextDefinition("language",
 *       label = @Translation("Language"),
 *       description = @Translation("If specified, the language used for getting the SMS message."),
 *       default_value = NULL,
 *       required = FALSE
 *     ),
 *   }
 * )
 *
 */
class MeliSendSms extends RulesActionBase implements ContainerFactoryPluginInterface {

  /**
   * The logger channel the action will write log messages to.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * @var SmsProviderInterface
   */
  protected $smsProvider;

  /**
   * Constructs a SystemSendsms object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The Rules logger channel.
   * @param SmsProviderInterface $sms_provider
   *   The mail manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelInterface $logger, SmsProviderInterface $sms_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
    $this->smsProvider = $sms_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('meli_sms'),
      $container->get('sms.provider')
    );
  }

  /**
   * Send SMS.
   *
   * @param string $number
   *   Sms addresses of the recipients.
   * @param string $message
   *   Sms message text.
   * @param \Drupal\Core\Language\LanguageInterface|null $language
   *   (optional) Language code.
   */
  protected function doExecute($number, $message, LanguageInterface $language = NULL) {
    $langcode = isset($language) ? $language->getId() : LanguageInterface::LANGCODE_SITE_DEFAULT;

    $sms_message = SmsMessage::create()
      ->setDirection(Direction::OUTGOING)
      ->setMessage($message)
      ->addRecipient($number);

    try {
      $this->smsProvider->send($sms_message);
      $this->logger->notice('Message is sent to @number: @message', [
        '@number' => $number,
        '@message' => $message,
      ]);
    }
    catch (\Exception $e) {
      $this->logger->error('Message could not be sent to @number: @error. Message: @message ', [
        '@number' => $number,
        '@message' => $message,
        '@error' => $e->getMessage(),
      ]);
    }

  }

}
