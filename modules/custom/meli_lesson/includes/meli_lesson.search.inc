<?php

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function meli_lesson_form_views_exposed_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Alter activity search form.

  /** @var \Drupal\views\ViewExecutable $view */
  $view = $form_state->getStorage()['view'];
  if ($view->id() != 'lessons') {
    return;
  }

  $form["keys"]["#attributes"]['placeholder'] = t('Search...');

  // Override All option label.
  if (isset($form["headphones"]["#options"]["All"])) {
    $form["headphones"]["#options"]["All"] = t('Any');
  }

  // Keep category and subcategory groups always open.
  $form["category_collapsible"]['#open'] = TRUE;
  $form["subcategory_collapsible"]['#open'] = TRUE;

  $form['category']['#options'] = _meli_lesson_category_options(0, FALSE);

  // Change category options to show category children only.
  $input = $form_state->getUserInput();
  $options = [];
  if (!empty($input['category'])) {
    $parents = $input['category'];
  }
  elseif (!empty($form['category']['#default_value'][0])) {
    $parents = $form['category']['#default_value'][0];
  }
  else {
    $parents = NULL;
  }

  if ($parents) {
    foreach ($parents as $parent) {
      $options += _meli_lesson_category_options($parent, FALSE);
    }
  }
  else {
    $form['subcategory']['#prefix'] = t('Select a category');
  }

  $form['subcategory']['#options'] = $options;
}
