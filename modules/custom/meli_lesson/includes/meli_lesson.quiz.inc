<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function meli_lesson_form_field_quiz_answer_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // TODO Add AJAX and override submit callback.
  $form['submit']['#ajax'] = [
    'callback' => 'meli_lesson_form_field_quiz_answer_form_ajax_callback',
    'wrapper' => 'field-quiz-wrapper',
  ];

  $form['submit']['#submit'][] = 'meli_lesson_form_field_quiz_answer_form_submit';

  $form['#prefix'] = '<div id="field-quiz-wrapper">';
  $form['#suffix'] = '</div>';

  $form['submit']['#weight'] = 100;
  $form['feedback'] = [
    '#type' => 'markup',
    '#markup' => '',
  ];

  // TODO Add class to right answer and selected answer and remove button.
  // See FieldQuizAnswerForm::formSubmit().
  $form_state_values = $form_state->cleanValues()->getValues();

  if (!empty($form_state_values)) {
    // $form['submit']['#disabled'] = TRUE;

    $error = FALSE;

    $items = $form['#items'];

    if (!empty($items)) {

      /** @var \Drupal\Core\Entity\ContentEntityBase $entity */
      $entity = $form['#entity'];
      $feedback = $entity->field_lesson_quiz_feedback->getValue();
      $form['feedback']['#markup'] = isset($feedback[0]['value']) ? ('<div class="lesson-quiz-feedback">' . $feedback[0]['value'] . '</div>'): '';

      foreach ($items as $delta => $item) {

        $item_values = $item->getValue();

        $is_wrong = FALSE;

        // Compare the submitted answers with the correct answers.
        // All correct answers should be answered correct -
        // there are no differences between correct and given answers allowed.
        // If the answer is available.
        if (isset($form_state_values['item_' . $delta])) {
          // If the answer is different with the correct one.
          if ($form_state_values['item_' . $delta] != $item_values['correct']) {
            $is_wrong = TRUE;
          }
        }
        else {
          // No answer available for this delta -
          // but should be there - error in the system.
          $is_wrong = TRUE;
        }

        if (isset($form['answers']['item_' . $delta])) {
          if ($item_values['correct']) {
            $form['answers']['item_' . $delta]['#wrapper_attributes']['class'][] = 'quiz-answer-correct';
          }
          elseif ($is_wrong) {
            $form['answers']['item_' . $delta]['#wrapper_attributes']['class'][] = 'quiz-answer-wrong';
          }
        }

        $error = $error || $is_wrong;
      }

    }

    //    $config = Drupal::config('field_quiz.settings');
    //     $form['answers']['#prefix'] = $error ? $config->get('test_answer_wrong') : $config->get('test_answer_correct');
  }
}

/**
 * Form AJAX callback.
 */
function meli_lesson_form_field_quiz_answer_form_ajax_callback(array &$form, FormStateInterface $form_state) {
  return $form;
}

/**
 * Form submit callback.
 */
function meli_lesson_form_field_quiz_answer_form_submit(&$form, FormStateInterface $form_state) {
  $form_state->setRebuild();
}
