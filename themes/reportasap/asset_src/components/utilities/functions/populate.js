// JSON Data retrieval
// function expects you to pass the URL endpoint that you wish to retrieve

import Promise from 'promise-polyfill';

// To add to window
if (!window.Promise) {
    window.Promise = Promise;
}
export function populate( endpoint ){

    return new Promise( function(resolve, reject){

        var xhttp = new XMLHttpRequest();
        xhttp.open( 'GET', endpoint, true);

        xhttp.onload = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {

                var response = JSON.parse(xhttp.responseText);
                resolve(response);

            }
            else {
                reject( Error( xhttp.statusText ) );
            }

        };

        // reject promises in case of network issues
        xhttp.onerror = function() {
            reject( Error ( 'There was a network error.' ) );
        };

        xhttp.send();

    });

}