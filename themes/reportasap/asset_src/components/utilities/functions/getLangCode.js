export function getLangCode(url) {

    // get last part of url (optional) or window
    var url = window.location.href;
    var langPart = url.split('/');
    
    var langCodes = ['el', 'es', 'hr', 'bg', 'it'];    
    
    var obj = '';

    // if query string exists
    if (langPart[3]) {
        
        if (langCodes.indexOf(langPart[3]) !== -1) {
            obj = langPart[3];
        }
        
    }

    return obj;
}