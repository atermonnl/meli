// React
import ReactDOM from 'react-dom';

var React = require('react');
var createReactClass = require('create-react-class');

import { populate } from './../utilities/functions/populate.js';
import { getLangCode } from './../utilities/functions/getLangCode.js';

// Node component
// view output of a single node
import Node from './node.jsx';

// Nodes
import Nodes from './nodes.jsx';

// Load more
import LoadMore from './loadmore.jsx';

var Loader = require('react-loader');

var NodesContainer =  createReactClass({

    // initial state of component
    getInitialState: function() {

        return({
            data : [],
            type : '',
            loaded : false,
            loadMore : false,
            page : 1,
            items_per_page : 9,
            langCode : ''
        })
    },

    componentDidMount: function(){

        var component = this;
        const componentContainer = document.getElementById('react-nodes');

        let langCode = getLangCode();

        let type = componentContainer.dataset.type;
        let endpoint = langCode ?  '/rest/' + type + '?_format=json&?langcode=' + langCode : '/rest/' + type + '?_format=json';

        populate( endpoint ).then(function( response ){
            let nodes = response.results;
            component.setState({
                data : nodes,
                count : response.count,
                loaded : true,
                loadMore : ( response.count > ( component.state.items_per_page * parseInt(component.state.page)  )&& type != 'partners') ? true : false,
                type : type,
                langCode : langCode
            });
        });
    },

    render: function() {

        var component = this;

        var options = {
            lines: 1,
            length: 0,
            width: 0
        };

        return (
            <div className={ "react--content " + component.state.type }>
                {!this.state.loaded &&
                  <Loader loaded={ this.state.loaded } options={options} />
                }
                <Nodes
                    data = { component.state.data }
                />
                <LoadMore
                    status = { component.state.loadMore }
                    onClicked = { this.processLoadmore }
                />
            </div>
        )
    },

    processLoadmore: function(){
        var component = this;

        component.setState({
            loaded: false
        });

        let page = component.state.page ? 'page=' + parseInt(component.state.page) : '';
        let new_page = parseInt(component.state.page) + 1;

        let type = component.state.type;

        let langcode = component.state.langCode ? 'langcode=' + component.state.langCode : '';

        let url_params = '?_format=json';
        if ( page && langcode ) {
            url_params = '&' + page + '&' + langcode;
        }
        else if ( page || langcode) {
            url_params = '&' + page + langcode;
        }

        let endpoint = '/rest/' + type + url_params;


        populate( endpoint ).then(function( response ){

          console.log(response, 'response');

            let nodes = response.results;

            let initialData = component.state.data;
            for ( let x = 0; x < nodes.length; x++ ) {
                initialData.push( nodes[x]);
            }

            component.setState({
                data : initialData,
                page : new_page,
                count : response.count,
                loaded : true,
                loadMore : ( response.count > ( component.state.items_per_page * (parseInt(new_page) + 1)  )&& type != 'partners') ? true : false,
                type : type
            });
        }).catch(err => console.log('There was an error:' + err));

    },

});


if ( document.getElementById('react-nodes') ) {

    var nodes_container = ReactDOM.render(
        <NodesContainer/>,
        document.getElementById('react-nodes')
    );

}
