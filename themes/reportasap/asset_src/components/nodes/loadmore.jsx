// React
var React = require('react');
var createReactClass = require('create-react-class');

// Single node
var LoadMore =  createReactClass({

    render() {

        if (this.props.status){
            return (
                <div className="align-center">
                    <div className="load-more">
                        <button className="button" onClick={ this.update } >Load more</button>
                    </div>
                </div>
            );
        }
        else
            return null;

    },

    // update function
    update: function() {

        this.props.onClicked();

    }

});


export default LoadMore;