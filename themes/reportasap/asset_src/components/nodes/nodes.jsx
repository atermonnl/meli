// React
var React = require('react');
var createReactClass = require('create-react-class');

import Node from './node.jsx';

// Posts component
var Nodes =  createReactClass({

    render() {

        // if we have a data array of posts
        if ( this.props.data.length > 0 ) {

            const posts = this.props.data.map( ( dataObject ) => {
                
                return (

                    <Node
                        key = { dataObject.nid.toString() }
                        title = { dataObject.title }
                        image = { dataObject.image }
                        link = { dataObject.path }
                        body = { dataObject.body }
                        date = { dataObject.date }
                    />

                )                

            });

            return (
                <div className="row">{ posts }</div>
            )

        }
        // otherwise
        else {
            return null;
        }

    },

});


export default Nodes;