// React
var React = require('react');
var createReactClass = require('create-react-class');

// Single node
var Node =  createReactClass({

    render() {
        
        return (
            <div className="col-lg-4">
                <article>

                    <div className="article--image">
                        <a href={ this.props.link }>
                            <img src= { this.props.image }  data-alt={ this.props.title } />
                        </a>
                    </div>

                    <div className="article--summary">
                        <a href={ this.props.link }>
                                <h4 className="article--title" dangerouslySetInnerHTML={ this.createMarkup( this.props.title ) } />
                        </a>

                        <div className="body--summary">
                            <p dangerouslySetInnerHTML={ this.createMarkup( this.props.body ) } />
                        </div>

                        <div className="date small">
                            <span dangerouslySetInnerHTML={ this.createMarkup( this.props.date ) } />
                        </div>
                    </div>

                </article>
            </div>
        );

    },

    createMarkup : function( html ) {
        return { __html: html };
    },

    componentDidMount: function() {
        new Imager('.delayed-landscape', {
            availableWidths: [360,780]
        });
    },    
        
});


export default Node;