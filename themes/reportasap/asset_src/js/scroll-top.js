window.addEventListener("scroll", function(event) {
    var top = window.scrollY //Modern Way (Chrome, Firefox)
     || window.pageYOffset 
     || document.documentElement.scrollTop;
    
    var scrollTopElements = document.querySelectorAll('.scrolltop');
    // if at least one does, loop over the returned elements and init
    if ( scrollTopElements.length ) {
        for ( var x = 0; x < scrollTopElements.length; x++ ) {
            if (top > 100){
                scrollTopElements[x].classList.add('visible');
            }
            else {
                scrollTopElements[x].classList.remove('visible');
            }                    
        }
    }
        
}, false);

// check if .scroll-top exists before running function
var scrollTopElements = document.querySelectorAll('.scrolltop');
// if at least one does, loop over the returned elements and init
if ( scrollTopElements.length ) {
    for ( var x = 0; x < scrollTopElements.length; x++ ) {

        // scroll top function
        scrollTopElements[x].addEventListener('click', function(e) {

            e.preventDefault();

            function scrollTo(element, to, duration) {
                if (duration <= 0) return;
                var difference = to - element.scrollTop;
                var perTick = difference / duration * 10;

                setTimeout(function() {
                    element.scrollTop = element.scrollTop + perTick;
                    if (element.scrollTop === to) return;
                    scrollTo(element, to, duration - 10);
                }, 10);
            }

            var scrollElement = document.body.scrollTop > 0 ? document.body : document.documentElement;
            scrollTo( scrollElement, 0, 600 );
            //scrollTo(document.body, 0, 600);

        }, false);

    };
}