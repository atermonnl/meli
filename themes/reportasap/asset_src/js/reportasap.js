/**
 * @file
 */
(function ($) {

  "use strict";
  Drupal.behaviors.reportasapTheme = {
    attach: function (context, settings) {

      //smooth anchors scrolling
      $(document).on('click', 'a[href^="#"]', function (event) {
            event.preventDefault();

            if ( $.attr(this, 'href') !== '#' ){
                $('html, body').animate({
                    scrollTop: $($.attr(this, 'href')).offset().top
                }, 500);
            }
        });

        //wrap iframes
        $("iframe").once('iframe').wrap("<div class='iframe-container'/>");

        //fix for views exposed form reset button
        $("input[id^='edit-reset']").click(function(event){
            event.preventDefault();
            location.reload();
        });


        $('.childmenu--toggle').on().click(function() {

            $(this).toggleClass('open');
            $(this).next('.child-menu').toggleClass('open');

            $(this).attr('aria-expanded') === true ? $(this).attr('aria-expanded', false) : $(this).attr('aria-expanded', true);
        });

        $(".meter > span").each(function() {
            $(this)
		.data("origWidth", $(this).width())
		.width(0)
		.animate({
			width: $(this).data("origWidth")
		}, 1200);
	});

    }
  }

  Drupal.behaviors.reportasapSlider = {
    attach: function (context, settings) {

        if ( document.getElementsByClassName('slideshow').length > 0 )
        {
            /*var dots = document.querySelectorAll('.slideshow-dot');
            dots[0].className = 'slideshow-dot active';*/

            var slides = document.querySelectorAll('.slideshow-item');
            slides[0].className = 'slideshow-item showing';

            if ( slides.length > 1 ) {
                var currentSlide = 0;
                var slideInterval = setInterval(nextSlide,5000);
            }
            /*for ( var x = 0; x < dots.length; x++ ) {
                dots[x].addEventListener('click', function(e) {
                    //pauseSlideshow();
                    goDotToSlide(this.dataset.position);
                });
            }*/
        }

        if ( document.getElementsByClassName('next--control').length > 0)
        {
            document.getElementsByClassName('next--control')[0].addEventListener('click', function(){
                //pauseSlideshow();
                nextSlide();
            });
        }

        if ( document.getElementsByClassName('prev--control').length > 0)
        {
            document.getElementsByClassName('prev--control')[0].addEventListener('click', function(){
                //pauseSlideshow();
                previousSlide();
            });
        }


        function nextSlide() {
            goToSlide(currentSlide+1);
        }

        function previousSlide() {
            goToSlide(currentSlide-1);
        }

        function goToSlide(n) {
            //dots[currentSlide].className = 'slideshow-dot';
            slides[currentSlide].className = 'slideshow-item';
            currentSlide = (n+slides.length)%slides.length;
            slides[currentSlide].className = 'slideshow-item showing';
            //dots[currentSlide].className = 'slideshow-dot active';
        }

        function goDotToSlide(n) { console.log(n);
            slides[currentSlide].className = 'slideshow-item';
            //dots[currentSlide].className = 'slideshow-dot';
            currentSlide = n;
            slides[n].className = 'slideshow-item showing';
            //dots[n].className = 'slideshow-dot active';
        }

        function pauseSlideshow() {
            clearInterval(slideInterval);
        }

    }
  }


  Drupal.behaviors.reportasapAccordion = {
    attach: function (context, settings) {

        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].onclick = function(){
                this.classList.toggle("active");
                this.nextElementSibling.classList.toggle("show");
            }
        }

    }
  }


})(jQuery);


