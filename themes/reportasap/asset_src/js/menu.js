// menu
var menuToggle = document.getElementById('menu--toggle');
var menu = document.getElementById('main-menu');

menuToggle.addEventListener('click', function(e) {
    e.preventDefault();

    if ( menuToggle.classList.contains('open') ) {
        menu.classList.remove('open');
        menuToggle.classList.remove('open');
        menuToggle.setAttribute( 'aria-expanded' , false );
    }

    else {
        menu.classList.add('open');
        menuToggle.classList.add('open');
        menuToggle.setAttribute( 'aria-expanded' , true );
    }

});


// sub nav drop down

var subToggles = document.getElementsByClassName('submenu--toggle');
for ( var x = 0; x < subToggles.length; x++ ) {

    subToggles[x].addEventListener('click', function(e) {
        e.preventDefault();
        e.target.parentNode.classList.toggle('open');
        e.target.parentNode.nextSibling.classList.toggle('open');
    });
}