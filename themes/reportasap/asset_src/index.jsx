/*
 *
 * Basic Theme
 *
 */
// styling
import './scss/base.scss';

// reportasap js
import './js/reportasap.js';
import './js/scroll-top.js';
import './js/menu.js';


/*
 *
 * Vendor Plugins
 *
 */

// imager - cover images
new Imager('.delayed-landscape', {
    availableWidths: [360,780,1200]
});

/*
 *
 * Components
 *
 */

import './components/nodes/nodes-container.jsx';
