// webpack
const webpack = require('webpack');

// extract text plugin
// required to generate a separate css file
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {

    mode: 'development',
    entry: [
            // main application root
            __dirname + '/asset_src/index.jsx'
    ],

    // destination after transformation
    output: {
            filename: 	'app.js',
            path: 		__dirname + '/asset_dist',
            publicPath: '/'
    },
    devtool: 'source-map',
    module: {
            rules: [
                {
                    test: 	/\.(js|jsx)$/,
                    exclude:	__dirname + '/node_modules/',
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                '@babel/preset-env',
                                '@babel/react'
                            ]
                        }
                    }
		},
                {
                    test: /\.scss$/,
                    use: [
                      MiniCssExtractPlugin.loader,
                      "css-loader",
                      "sass-loader",
                    ]
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/,
                    loader : 'file-loader?name=[name].[ext]&publicPath=/images/&outputPath=images/'
		}
            ]
    },
    plugins: [new MiniCssExtractPlugin({filename: 'app.css'})]
}
